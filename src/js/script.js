"use strict";

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
];

class Book {
    constructor (author, name, price) {
        this.author = author;
        this.name = name;
        this.price = price;
    }

    render() {
        const container  =  document.getElementById('root');
        const list = `        
        <ul>
            <li>${this.author}</li>
            <li>${this.name}</li>
            <li>${this.price}</li>
        </ul>`;
        container.insertAdjacentHTML('beforeend', list);
    }
}

class ObjectError extends Error {
    constructor(property) {
        super(`${property} property is not defined or incorrect`);
        this.name = 'ObjectError';
    }
}

books.forEach((book) => {
    try {
        const author = book.author;
        const name = book.name;
        const price = book.price;

        if (price !== undefined && name !== undefined && author !== undefined) {
            new Book(author, name, price).render();
        } else {
            if (author === undefined) {
                throw new ObjectError('author');
            } else if (name === undefined) {
                throw new ObjectError('name');
            } else if (price === undefined) {
                throw new ObjectError('price');
            }
        }
    } catch (err) {
        console.error(err);
    }
});
